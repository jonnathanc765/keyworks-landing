const common = require('./webpack.common');
const merge = require("webpack-merge");
const path = require("path");
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
    mode: "development",
    devtool: "none",
    module: {
        rules: [{
            test: /\.s[ac]ss$/i,
            use: ['style-loader', 'css-loader', 'sass-loader'],
            
        },
        {
            test: /\.(ttf|woff|eot|svg|gif|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: [{
              loader: 'file-loader',
              options: {
                name: "[name].[hash].[ext]",
                outputPath: "img",
                esModule: false
              }
            }, ],
          }]
    },
    plugins: [new HtmlWebpackPlugin({
        filename: 'index.html',
        template: "./src/index.html"
    })],
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, 'dist')
    }
});